package com.spartez;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class App {

    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final HttpClient httpClient = HttpClient.newBuilder().executor(executor).build();

    public static void main(String[] args) throws URISyntaxException, InterruptedException {
        final App app = new App();
        measureTime(app::runAsync, "async");
        measureTime(app::runSync, "sync");
        app.executor.shutdown();
    }

    private static void measureTime(Runnable runnable, String name) {
        final long time = System.currentTimeMillis();
        runnable.run();
        final long totalTime = System.currentTimeMillis() - time;
        System.out.println(String.format("Total time of %s: %d", name, totalTime));
    }

    private static HttpRequest buildRequest(long n) {
        try {
            return HttpRequest
                    .newBuilder(new URI(String.format("http://localhost:8080/%d", n)))
                    .GET()
                    .build();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private void runAsync() {
        final List<CompletableFuture<Long>> squares = LongStream.range(1, 10)
                .mapToObj(num ->
                        httpClient.sendAsync(buildRequest(num), HttpResponse.BodyHandler.asString())
                                .thenApplyAsync(resp -> getSquareFromResponse(num, resp), executor))
                .collect(Collectors.toList());
        final CompletableFuture<List<Long>> squares2 = sequence(squares, executor);
        squares2.thenAcceptAsync(this::writeSum, executor).join();
    }

    private void runSync() {
        final List<Long> list = LongStream.range(1, 10)
                .mapToObj(num -> {
                    final HttpResponse<String> resp;
                    try {
                        resp = httpClient.send(buildRequest(num), HttpResponse.BodyHandler.asString());
                    } catch (IOException | InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    return getSquareFromResponse(num, resp);
                }).collect(Collectors.toList());
        writeSum(list);
    }

    private Long getSquareFromResponse(long num, HttpResponse<String> resp) {
        final Long val = Long.valueOf(resp.body());
        System.out.println(String.format("%d^2 = %d", num, val));
        return val;
    }

    private void writeSum(List<Long> list) {
        final LongSummaryStatistics sum = list.stream().collect(Collectors.summarizingLong(i -> i));
        System.out.println(String.format("Total sum: %d", sum.getSum()));
    }

    private static <U> CompletableFuture<List<U>> sequence(List<CompletableFuture<U>> list, ExecutorService executor) {
        CompletableFuture<List<U>> acc = CompletableFuture.completedFuture(new ArrayList<>());
        for (CompletableFuture<U> future : list) {
            acc = acc.thenCombineAsync(future, (a, el) -> {
                a.add(el);
                return a;
            }, executor);
        }
        return acc;
    }

}
