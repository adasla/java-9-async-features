package com.spartez;

import io.reactivex.Flowable;
import io.reactivex.netty.RxNetty;
import io.reactivex.netty.protocol.http.AbstractHttpContentHolder;
import rx.Observable;

import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.LongSummaryStatistics;
import java.util.stream.Collectors;

import org.reactivestreams.Subscriber;

public class App {

	public static void main(String[] args) throws URISyntaxException, InterruptedException {
		final App app = new App();
		measureTime(app::runRx, "rx");
	}

	private static void measureTime(Runnable runnable, String name) {
		final long time = System.currentTimeMillis();
		runnable.run();
		final long totalTime = System.currentTimeMillis() - time;
		System.out.println(String.format("Total time of %s: %d", name, totalTime));
	}

	private void runRx() {
		LongSummaryStatistics summaryStatistics = Flowable.range(1, 10).flatMap((Integer num) -> {
			System.out.println(num);
			final Observable<Long> longObservable = RxNetty
					.createHttpGet(String.format("http://localhost:8080/%d", num))
					.flatMap(AbstractHttpContentHolder::getContent).map(cont -> cont.toString(Charset.defaultCharset()))
					.map(Long::valueOf);
			return (Subscriber<? super Long> subscriber) -> {
				longObservable.subscribe((Long l) -> {
					System.out.println(String.format("%d^2 = %d", num, l));
					subscriber.onNext(l);
				});
			};
		})
				.take(10)
				.toList()
				.blockingGet()
				.stream().collect(Collectors.summarizingLong(i -> i));
		System.out.println(String.format("Total sum: %d", summaryStatistics.getSum()));
	}

}
