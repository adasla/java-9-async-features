package com.spartez.springhttp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.LongSummaryStatistics;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_PLAIN;

@Configuration
@EnableAutoConfiguration
public class SpringHttpApplication {
    private static final Logger logger = LoggerFactory.getLogger(SpringHttpApplication.class);
    private static ConfigurableApplicationContext ctx;
    private WebClient webClient;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SpringHttpApplication.class);
        app.setWebApplicationType(WebApplicationType.NONE);
        ctx = app.run(args);
    }

    private static void measureTime(Runnable runnable, String name) {
        final long time = System.currentTimeMillis();
        runnable.run();
        final long totalTime = System.currentTimeMillis() - time;
        System.out.println(String.format("Total time of %s: %d", name, totalTime));
    }

    @Bean
    ApplicationRunner myRunner() {
        return args -> {
            webClient = WebClient.create();
            measureTime(this::runWebClient, "web client");
        };
    }

    private void runWebClient() {
        LongSummaryStatistics summaryStatistics = Flux.range(1, 10)
                .flatMap(i -> webClient
                        .get()
                        .uri("http://localhost:8080/" + i)
                        .exchange()
                        .flatMap(resp -> resp.bodyToMono(String.class)))
                .collect(Collectors.summarizingLong(Long::valueOf))
                .block();

        System.out.println(String.format("Total sum: %d", summaryStatistics.getSum()));
    }

}
